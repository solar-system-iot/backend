const express = require("express"),
    cors = require("cors"),
    app = express(),
    PORT = process.env.PORT || 8080,
    mongoose = require("mongoose"),
    PlanetInfo = require("./models/planetInfoModel"); //created PlanetInfo mongodb model

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
const url = `mongodb+srv://wirelessteam6:1234@cluster0.qqrtd.mongodb.net/backend_db?retryWrites=true&w=majority`;
mongoose
  .connect(url)
  .then(() => {
    console.log("Connected to database ");
  })
  .catch((err) => {
    console.error(`Error connecting to the database. \n${err}`);
  });

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

var planet_routes = require("./routes/planetInfoRoutes"); //importing planetInfo route
planet_routes(app); //register the routes
var today_routes = require("./routes/todayInfoRoutes"); //importing planetInfo route
today_routes(app); //register the routes

app.listen(PORT, () => console.log(`it's alive on http://localhost:${PORT}`));

app.get("/home", (req, res) => {
  res.status(200).send({
    welcome: "⭐⭐⭐ welcome to SOLAR-SYSTEM-PROJ! ⭐⭐⭐",
    greeting: "Hello, explorer! Welcome to our site.",
  });
});

app.post("/greet/:name", (req, res) => {
  const { name } = req.params;
  const { message } = req.body;

  if (!message) {
    res.status(418).send({
      message: "Please give a user greeting message.",
    });
  }
  res.send({
    greeting: `${message} from ${name}.`,
  });
});
