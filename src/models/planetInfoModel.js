'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlanetInfoSchema = new Schema({
    planetName: {
        type: String,
        require: 'Planet name is required.'
    },
    terrestrial_date: {
        type: Date,
        require: 'Date is required.'
    },
    sol: Number,
    ls: Number,
    month: Number,
    min_temp: Number,
    max_temp: Number,
    all_min_t: Number,
    all_max_t: Number,
    pressure: Number,
    min_pressure: Number,
    max_pressure: Number,
    wind_speed: Number,
    atmo_opacity: String,
    O_a: Number,
    N_a: Number,
    Ar_a: Number,
    CO2_a: Number,
    other_a: Number,
    O_g: Number,
    Si_g: Number,
    Al_g: Number,
    Fe_g: Number,
    Ca_g: Number,
    Na_g: Number,
    K_g: Number,
    Mg_g: Number,
    Cl_g: Number,
    other_g: Number,
});

module.exports = mongoose.model('PlanetInfo', PlanetInfoSchema);