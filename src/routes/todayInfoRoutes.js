'use strict';
module.exports = function(app) {
    var todayInfo = require('../controllers/todayInfoController');

    //todayInfo Routes
    app.route('/today')
        .get(todayInfo.read_today);
};