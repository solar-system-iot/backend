'use strict';
module.exports = function(app) {
    var planetInfo = require('../controllers/planetInfoController');

    //planetInfo Routes
    app.route('/info')
        .get(planetInfo.list_all_info)
        .post(planetInfo.create_info);
    
    app.route('/info/:infoId')
        .get(planetInfo.read_info)
        .put(planetInfo.update_info)
        .delete(planetInfo.delete_info);

    app.route('/planet/:planetName')
        .get(planetInfo.read_by_name);
};