'use strict';

const planetInfoRoutes = require('../routes/planetInfoRoutes');

var mongoose = require('mongoose'),
    PlanetInfo = mongoose.model('PlanetInfo');

exports.list_all_info = (req, res) => {
    PlanetInfo.find({}, (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};

exports.create_info = (req, res) => {
    var new_info = new PlanetInfo(req.body);
    new_info.save( (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};

exports.read_info = (req, res) => {
    PlanetInfo.findById(req.params.infoId, (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};

exports.update_info = (req, res) => {
    PlanetInfo.findByIdAndUpdate({_id: req.params.infoId}, req.body, {new: true}, (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};

exports.delete_info = (req, res) => {
    PlanetInfo.deleteOne({_id: req.params.infoId}, (err, info) => {
        if (err){
            res.send(err);
        }
        res.json({
            message: `Planet's information has been deleted`
        });
    });
};

exports.read_by_name = (req, res) => {
    PlanetInfo.find({'planetName' : req.params.planetName}, (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};