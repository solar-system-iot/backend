'use strict';

const moment = require('moment');
const planetInfoRoutes = require('../routes/todayInfoRoutes');

var mongoose = require('mongoose'),
    PlanetInfo = mongoose.model('PlanetInfo');

exports.read_today = (req, res) => {
    const day = new Date(req.query.date)
    const today = moment(day).startOf('day')
    const date = {
        $gte: today.toDate(),
        $lte: moment(today).endOf('day').toDate()
      }
    var filter = {terrestrial_date: date, planetName: req.query.name} ;
    if ( !req.query.name ) {
        filter = {terrestrial_date: date};
    }
    PlanetInfo.aggregate([
        { $match: filter },
        {
            $addFields: { avg_temp:
                { $avg: [ "$min_temp", "$max_temp" ] } }
        }
    ], (err, info) => {
        if (err) {
            res.send(err);
        }
        res.json(info);
    });
};
